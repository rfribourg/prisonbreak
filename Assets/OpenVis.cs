using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenVis : MonoBehaviour
{
    public float translationSpeed = 150f; 

    private void OnTriggerEnter(Collider other)
    {
        // Check if the collision was with a screwdriver
        if (other.gameObject.tag == "vis")
        {
            other.transform.Translate(new Vector3(0, 1, 0) * translationSpeed * Time.deltaTime);         }
    }
    private void OnTriggerExit(Collider other)
    {
        // Check if the collision was with a screwdriver
        if (other.gameObject.tag == "vis")
        {
            
            other.gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
    }
}

