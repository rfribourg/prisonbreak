Les intéractions possibles  les manettes, grab, clic droit 
La navigation  les boules de la manettes, droite  avancer ; gauche  tourner la vue (utilisation de prefab PlayerController d’oculus) 
Interaction cadenas  touche A et touche B sont activés lorsqu’on est assez proche du cadenas 
Interaction puzzle  Grab des pièces 
Interaction CellMate  Appuie de la manette pour générer un dialogue
Interaction boite encastrée  grab le tournevis et dévisser les vis 
Interaction pièce de papier  grab la lampe et allume le mur pour lire l’énigme 
Inventory  apparition lorsqu’on clique sur Y 
Il y a aussi un autre avatar qui est le gardien (vous pouvez trouver les détails de son implémentation dans le compte rendu) 
Les indices sont par ordre  Boîte(3), Lampe(0), Puzzle (4), CellMate(2)

Améliorations possibles
    -Utiliser une téléportation pour la navigation ( nausée )
    -Amélioration ou changement total des énigmes qui existent
    -Implémenter le script de shader pour l’énigme de la lampe
    -Voir la dernière partie du rapport. 

